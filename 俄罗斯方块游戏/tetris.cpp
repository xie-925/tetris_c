///////////////////////////////////////////////////
//	程序名称：俄罗斯方块
//	编译环境：Visual Studio 2019，EasyX
//	编写人员：谢成龙 & 王秀祎
//	编写日期：2020/06/15 -- 2020/07/01
//	
///////////////////////////////////////////////////
#define _CRT_SECURE_NO_WARNINGS
#include <easyx.h>
#include <conio.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <windows.h>
#include <mmsystem.h>
#pragma comment(lib,"winmm.lib")

////////////////////////////////////
//函数声明区域
void init();											//初始化界面
void creablock(int x, int y, COLORREF c, int cleark);	//新建单个方块
void creablocks(int n, int clear);						//新建整体方块
void creablocks_t(int n, int clear);					//存放下一个方块
void Down();											//下落
void moveleft();										//键盘左移
void moveright();										//键盘右移
void revolve();											//方块旋转
void Newblock();										//生成方块开始下落
void Map();												//记录方块到地图中
void sink();											//执行沉底操作
void _exit();											//退出
void welcome();											//欢迎界面
void save();											//保存用户信息
void Register();										//注册信息
void s_p(int score);									//更新当前和历史分数

///////////////////////////////////
//全局变量定义
DWORD mtime;						//定义时间变量(在生成方块下落时使用,用于控制下落时间)
int block_x = 100;					//方块的全局横坐标	
int block_y = 0;					//方块的全局纵坐标
int shape;							//方块的随机形状7个(0-6)
int shape_t;						//下一个形状的随机数
int dire = 0;						//方块的旋转方向4个(0-3)
int map[11][16];					//全局地图坐标点
COLORREF mapc[11][16];				//全局地图的颜色值
int score = 0;						//分数初始化
int M_score = 0;					//历史最高分初始化

//存放用户信息的结构体(包括用户名、地图坐标和颜色、分数）
struct visitor
{
	char id[20];
	int v_map[11][16];
	COLORREF v_mapc[11][16];
	int v_score;
}visit;

//存放方块的结构体
struct block
{
	int dir[4][4][2];				// 方块的坐标位置 
	COLORREF color;					// 方块的颜色
};
block blocks[7] = {
	{ 0, -2, 0, -1, 0, 0, 1, 0, 0, -1, 1, -1, 2, -1, 0, 0, 0, -2, 1, -2, 1, -1, 1, 0, 0, 0, 1, 0, 2, 0, 2, -1 ,RED},		//  L 型
	{ 0, 0, 1, 0, 1, -1, 1, -2, 0, -1, 0, 0, 1, 0, 2, 0, 0, -2, 0, -1, 0, 0, 1, -2, 0, -1, 1, -1, 2, -1, 2, 0 ,BLUE},       //  L 型（反）
	{ 0, -1, 0, 0, 1, -1, 1, 0, 0, -1, 0, 0, 1, -1, 1, 0, 0, -1, 0, 0, 1, -1, 1, 0, 0, -1, 0, 0, 1, -1, 1, 0 ,GREEN},		// 田 型
	{ 0, 0, 1, -1, 1, 0, 2, 0, 1, -2, 1, -1, 1, 0, 2, -1, 0, -1, 1, -1, 1, 0, 2, -1, 0, -1, 1, -2, 1, -1, 1, 0 ,YELLOW},	// 山 型
	{ 1, -3, 1, -2, 1, -1, 1, 0, 0, -2, 1, -2, 2, -2, 3, -2, 1, -3, 1, -2, 1, -1, 1, 0, 0, -2, 1, -2, 2, -2, 3, -2 ,CYAN},	// 一 型
	{ 0, -1, 1, -1, 1, 0, 2, 0, 0, -1, 0, 0, 1, -2, 1, -1, 0, -1, 1, -1, 1, 0, 2, 0, 0, -1, 0, 0, 1, -2, 1, -1 ,BROWN},		//  Z 型
	{ 0, 0, 1, -1, 1, 0, 2, -1, 1, -2, 1, -1, 2, -1, 2, 0, 0, 0, 1, -1, 1, 0, 2, -1, 1, -2, 1, -1, 2, -1, 2, 0 ,MAGENTA}	//  Z 性（反）
};
/*
	使用了3维数组来描述方块的坐标位置,将方块分成4种旋转形态,
	这4种形态中的每种通过4个方块组成,每个方块又对应两个X,Y坐标,
	即需要4*4*2=32个数值
*/

//主函数
int main(void)
{
	welcome();
	return 0;
}

//欢迎界面
void welcome()
{
	initgraph(600, 400);
	setorigin(0, 0);
	//背景：（自动反差色？）
	int bc = 0x50A2FF;		//蓝
	setbkcolor(1 - bc);		//1-蓝 = 橙（反差色）
	cleardevice();

	//文字：
	settextcolor(bc);		//橙
	settextstyle(45, 30, 0, 0, 0, 300, false, false, false);		//30长 30宽 500粗细
	setbkmode(TRANSPARENT);
	outtextxy(70, 100, _T("Welcome to Tetris"));

	//按钮：height:30	width:100
	setfillcolor(YELLOW);
	setlinecolor(RED);
	setlinestyle(PS_SOLID, 4);		//实线，weight：5px
	fillrectangle(250, 185, 350, 215);		//按钮(上)
	fillrectangle(250, 250, 350, 280);		//按钮(下)

	//按钮字：
	settextcolor(RED);
	settextstyle(15, 15, 0, 0, 0, 0, false, false, false);
	outtextxy(257, 193, _T("START"));		//按钮字(上)
	outtextxy(268, 258, _T("QUIT"));

	//鼠标事件们：
	MOUSEMSG mouse;		//鼠标类型变量
	while (true)
	{
		mouse = GetMouseMsg();		//获得鼠标信息
		if (mouse.x > 250 && mouse.x < 350 && mouse.y>185 && mouse.y < 215 && mouse.mkLButton)		//start
		{
			setlinestyle(PS_SOLID, 6);				
			fillrectangle(251, 186, 351, 216);		
			outtextxy(258, 194, _T("START"));		
			//以上是点击之后的效果
			Sleep(600);
			FlushMouseMsgBuffer();
			Register();
		}
		if (mouse.x > 250 && mouse.x < 350 && mouse.y>250 && mouse.y < 280)			//quit
			if (mouse.mkLButton)
			{
				Sleep(500);
				FlushMouseMsgBuffer();
				exit(0);
			}
	}
	return;
}

//注册用户名
void Register()
{
	FILE* fp1, * fp2;
	fp1 = fopen("documents\\id.txt", "rb");
	char last[20];
	memset(last, 0, sizeof(last));
	fread(last, sizeof(last), 1, fp1);
	memset(visit.id, 0, sizeof(visit.id));
	InputBox((wchar_t*)visit.id, 50, L"请输入您的用户名:");		 //输入用户名
	if (strcmp(visit.id, last) == 0)
	{
		HWND wnd = GetHWnd();
		if (MessageBox(wnd, _T("检测到有您的用户信息，是否继续上一局游戏？"), _T("有存档"), MB_YESNO | MB_ICONQUESTION) == IDYES)//提示信息是否保存进度
		{
			fread(map, sizeof(map), 1, fp1);
			fread(mapc, sizeof(mapc), 1, fp1);
			fread(&score, sizeof(score), 1, fp1);
			init();
		}
		else
		{
			fp2 = fopen("documents\\id.txt", "wb");
			fwrite(visit.id, sizeof(visit.id), 1, fp2);
			init();
		}
	}
	else
	{
		fp2 = fopen("documents\\id.txt", "wb");
		fwrite(visit.id, sizeof(visit.id), 1, fp2);
		init();
	}
}

//初始化游戏界面
void init()
{
	initgraph(600, 440);						//建立窗口
	srand((unsigned)time(NULL));				//设置随机，使随机数为真随机
	HWND hwnd = GetHWnd();
	SetWindowText(hwnd, L"俄罗斯方块");			//设置窗口的标题
	setbkcolor(RGB(135, 206, 250));				//设置背景颜色
	cleardevice();								//填充
	setorigin(22, 20);							//设置游戏区域原点
	//文字说明部分
	settextstyle(16, 0, _T("宋体"));
	outtextxy(375, 265, _T("操作说明"));
	outtextxy(350, 290, _T("上键(W)：旋转"));
	outtextxy(350, 310, _T("左键(A)：左移"));
	outtextxy(350, 330, _T("右键(D)：右移"));
	outtextxy(350, 350, _T("下键(S)：下移"));
	outtextxy(350, 370, _T("ESC键  ：退出"));
	outtextxy(360, 0, _T("下一个："));
	outtextxy(350, 170, _T("用户："));
	outtextxy(400, 170, (wchar_t*)visit.id);	//输出用户名
	s_p(score);									//显示当前和最高分数
	rectangle(350, 20, 450, 120);				//下一个方块区域
	setlinecolor(WHITE);						//边框颜色
	rectangle(-3, -2, 273, 398);				//游戏矩形区域
	setbkmode(TRANSPARENT);						//设置背景颜色为透明
	Newblock();
	_getch();									//暂停窗口
}

// 游戏结束
void _exit()
{
	HWND wnd = GetHWnd();
	if (MessageBox(wnd, _T("是否保存当前进度！"), _T("退出"), MB_YESNO | MB_ICONQUESTION) == IDYES)//提示信息是否保存进度
	{
		save();
		exit(0);
	}
	else
		exit(0);
}

// 绘制全局地图
void Map()
{
	for (int i = 0; i < 11; i++)
		for (int j = 0; j < 16; j++)
		{
			if (map[i][j] == 1)
				creablock(i * 25, j * 25, mapc[i][j], 0);
			if (map[i][j] == 0)
				creablock(i * 25, j * 25, mapc[i][j], 1);
		}
}


//保存用户信息存入文件
void save()
{
	for (int i = 0; i < 11; i++)
		for (int j = 0; j < 16; j++)
		{
			visit.v_map[i][j] = map[i][j];
			visit.v_mapc[i][j] = mapc[i][j];
		}
	FILE* fp;
	fp = fopen("documents\\id.txt", "wb");
	fwrite(visit.id, sizeof(visit.id), 1, fp);
	fwrite(visit.v_map, sizeof(visit.v_map), 1, fp);
	fwrite(visit.v_mapc, sizeof(visit.v_mapc), 1, fp);
	fwrite(&visit.v_score, sizeof(visit.v_score), 1, fp);
}

//分数
void s_p(int score)
{
	visit.v_score = score;
	wchar_t  score_tips[30];
	clearrectangle(350, 190, 500, 210);						//清除上一条分数
	swprintf(score_tips, 29, L"当前分数：%d", score);		
	outtextxy(350, 190, score_tips);						//更新分数
	FILE* fp1, * fp2;
	fp1 = fopen("documents\\max.txt", "rb");
	fread(&M_score, sizeof(M_score), 1, fp1);
	if (M_score < score)
		M_score = score;
	wchar_t  M_score_tips[10];
	clearrectangle(350, 210, 500, 230);						//清除上一条历史最高分
	swprintf(M_score_tips, 9, L"历史最高：%d", M_score);	//更新历史最高分
	outtextxy(350, 210, M_score_tips);
	fp2 = fopen("documents\\max.txt", "wb");
	fwrite(&M_score, sizeof(M_score), 1, fp2);
}

//新建单个方块
void creablock(int x, int y, COLORREF c, int clear)
{
	if (clear == 0)
	{
		setfillcolor(c);
		fillroundrect(x, y, x + 20, y + 20, 5, 5);	//新建
	}
	else
	{
		clearrectangle(x, y, x + 20, y + 20);		//清除区域
	}
}

//新建整体方块 (第二个参数为0则正常新建 非0则为清除区域)
void creablocks(int n, int clear)
{
	HRGN rgn = CreateRectRgn(0, 20, 300, 420);		// 创建一个矩形区域
	setcliprgn(rgn);								// 将该矩形区域设置为裁剪区
	DeleteObject(rgn);								// 不再使用 rgn，清理 rgn 占用的系统资源
	creablock(block_x + (blocks[n].dir[dire][0][0]) * 25, block_y + (blocks[n].dir[dire][0][1]) * 25, blocks[n].color, clear);
	creablock(block_x + (blocks[n].dir[dire][1][0]) * 25, block_y + (blocks[n].dir[dire][1][1]) * 25, blocks[n].color, clear);
	creablock(block_x + (blocks[n].dir[dire][2][0]) * 25, block_y + (blocks[n].dir[dire][2][1]) * 25, blocks[n].color, clear);
	creablock(block_x + (blocks[n].dir[dire][3][0]) * 25, block_y + (blocks[n].dir[dire][3][1]) * 25, blocks[n].color, clear);
	setcliprgn(NULL);								// 取消之前设置的裁剪区
}

/*
	新建整体方块的时候是通过调用4个建单独小方块的函数将方块的形状,横纵坐标的参数和颜色的参数传递进去
	在调用新建大方块函数的时候,只需要将某个形状说明就行.
*/

//用于存放下一个即将生成的方块
void creablocks_t(int n, int clear)
{
	creablock(353 + (blocks[n].dir[0][0][0]) * 25, 97 + (blocks[n].dir[0][0][1]) * 25, blocks[n].color, clear);
	creablock(353 + (blocks[n].dir[0][1][0]) * 25, 97 + (blocks[n].dir[0][1][1]) * 25, blocks[n].color, clear);
	creablock(353 + (blocks[n].dir[0][2][0]) * 25, 97 + (blocks[n].dir[0][2][1]) * 25, blocks[n].color, clear);
	creablock(353 + (blocks[n].dir[0][3][0]) * 25, 97 + (blocks[n].dir[0][3][1]) * 25, blocks[n].color, clear);
}


//生成方块开始下落
void Newblock()
{
	Map();								//每次生成方块时先重新绘制地图
	block_x = 100;	block_y = 0;		//重新初始化方块的横纵坐标
	shape = shape_t;					//将下一个方块的随机形状给当前变量
	creablocks_t(shape_t, 1);			//擦除下一个方块
	//shape_t = rand() % 7;				//重新生成随机形状
	shape_t = ;				//重新生成随机形状
	creablocks(shape, 0);				//新建当前方块
	creablocks_t(shape_t, 0);			//新建下一个方块

	while (true)
	{
		DWORD newtime = GetTickCount();	//控制执行时间
		if (newtime - mtime >= 500)
		{
			mtime = newtime;
			Down();
		}
		if (_kbhit())
		{
			switch (_getch())
			{
			case 'w':
			case 'W':
			case 72:	revolve(); break;
			case 'a':
			case 'A':
			case 75:	moveleft(); break;
			case 'd':
			case 'D':
			case 77:	moveright(); break;
			case 's':
			case 'S':
			case 80:	Down(); break;
			case 27:	_exit(); break;
			}
		}
		Sleep(20); //加点延时 据说可以降低CPU占用
	}
}

//检查方块是否越界
int checkblock(int x, int y, int dir)
{
	int ch_x[4], ch_y[4];
	int i, n = 0;
	for (i = 0; i < 4; i++)
	{
		ch_x[i] = x + (blocks[shape].dir[dir][i][0]) * 25;
		ch_y[i] = y + (blocks[shape].dir[dir][i][1]) * 25;
	}
	for (i = 0; i < 4; i++)
	{
		if (ch_x[i] < 0 || ch_x[i] > 250 || ch_y[i] > 375)		//判断
			return 1;
		if (ch_y[i] < 0) // 当下落后再开始判断
			continue;
		if (map[ch_x[i] / 25][ch_y[i] / 25] == 1)
			return 1;
	}
	return 0;
}

/*
	检查方块是否越界，函数有3个形参，分别用于接收方块的横纵坐标和当前的旋转状态，
	通过两个数组分别存放每个单独小方块的坐标，判断单独方块的横纵坐标是否与边界相撞。
*/

//方块落地
void sink()
{
	int x[4], y[4], i, j;
	for (i = 0; i < 4; i++)
	{
		x[i] = block_x / 25 + (blocks[shape].dir[dire][i][0]);
		y[i] = block_y / 25 + (blocks[shape].dir[dire][i][1]);
	}
	for (i = 0; i < 4; i++)
	{
		if (y[i] >= 0)
		{
			map[x[i]][y[i]] = 1;
			mapc[x[i]][y[i]] = blocks[shape].color;
		}
	}
	/*
		将map地图全部初始化为0后,定义两个数组,分别用于存放每个小方块的x,y坐标,然后将对应的
		map地图上的元素点也就是二维数组中的值赋为1.最后在开始新建下一个大方块前将对应的方块和颜色画到地图中.
	*/
	int mapT[11][16], jT = 15;
	COLORREF mapcT[11][16];
	memset(mapT, 0, sizeof(mapT));
	memset(mapcT, 0, sizeof(mapcT));
	for (j = 15; j >= 0; j--)
	{
		int n = 0;
		for (i = 0; i < 11; i++)
		{
			if (map[i][j] == 1)
				n++;
		}
		if (n != 11)
		{
			for (i = 0; i < 11; i++)
			{
				mapT[i][jT] = map[i][j];
				mapcT[i][jT] = mapc[i][j]; mapc[i][j];
			}
			jT--;
		}
		else
		{
			score++;
			PlaySound(L"documents\\clear.wav", NULL, SND_FILENAME | SND_ASYNC);
			s_p(score);
		}
	}
	/*
		定义临时地图数组,用于存放地图的元素点和颜色值,利用循环将地图中的元素依次判断,当某一行不全为1的时候
		把这一行赋给临时数组,某一行全为1时不进行操作
	*/
	for (j = 0; j < 16; j++)
	{
		for (i = 0; i < 11; i++)
		{
			map[i][j] = mapT[i][j];
			mapc[i][j] = mapcT[i][j];
		}
	}
	// 最后将临时数组的元素全部赋回给地图,每次新建方块之前重新绘制地图
	for (i = 0; i < 11; i++)
	{
		HWND wnd = GetHWnd();
		if (map[i][0] == 1)
		{
			PlaySound(L"documents\\over.wav", NULL, SND_FILENAME | SND_ASYNC);
			MessageBox(wnd, _T("GameOver!!!"), _T("游戏结束"), MB_OK);//提示信息是否保存进度
			exit(0);
		}
	}
	// 当地图的最上方存在1时游戏结束
	Newblock();
}

//下落
void Down()
{
	if (checkblock(block_x, block_y + 25, dire) == 0)
	{
		creablocks(shape, 1);
		block_y += 25;
		creablocks(shape, 0);
	}
	else
	{
		sink();
	}
}

//左移
void moveleft()
{
	if (checkblock(block_x - 25, block_y, dire) == 0)
	{
		creablocks(shape, 1);
		block_x -= 25;
		creablocks(shape, 0);
	}
}

//右移
void moveright()
{
	if (checkblock(block_x + 25, block_y, dire) == 0)
	{
		creablocks(shape, 1);
		block_x += 25;
		creablocks(shape, 0);
	}
}

//旋转
void revolve()
{
	for (int i = 1; i <= 3; i++)
		if (checkblock(block_x, block_y, (dire + i) % 4) == 0)
		{
			creablocks(shape, 1);
			dire = (dire + i) % 4;
			creablocks(shape, 0);
			PlaySound(L"documents\\revolve.wav", NULL, SND_FILENAME | SND_ASYNC);
			break;
		}
}
//旋转时将所有旋转状态都进行判断,而不是依次按顺序旋转,为了避免dire下标越界,将dire对4取余.

